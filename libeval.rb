class NarratorGUI
  def openEval
    if @codewin != nil and @coutwin != nil
      @codewin.destroy
      @coutwin.destroy
    else
      @codewin = Gtk::Window.new
      @codes = Gtk::ScrolledWindow.new
      ruby_lang = Gtk::SourceLanguageManager.new.get_language('ruby')
      revalbuf = Gtk::SourceBuffer.new ruby_lang
      @cbox = Gtk::SourceView.new revalbuf
      @cbox.show_line_marks = true
      @cbox.show_line_numbers = true
      @cbox.wrap_mode = Gtk::TextTag::WRAP_WORD
      @codes.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
      @coutwin = Gtk::Window.new
      @couts = Gtk::ScrolledWindow.new
      @cout = Gtk::TextView.new
      @cout.wrap_mode = Gtk::TextTag::WRAP_WORD
      @couts.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
      @codewin.title = "Live Evaluation"
      @codewin.set_size_request(500, 450)
      @coutwin.set_size_request(450, 200)
      @codewin.signal_connect('destroy') { @codewin = nil }
      @code = Gtk::Table.new 5, 6, false
      @cout.buffer.create_tag("err",           {"underline"     => Pango::Underline::SINGLE, "foreground" => "#ff0000"})
      @coutwin.add @couts
      @couts.add @cout
      @coutwin.title = "Eval Output"
      @coutwin.show_all
      @codewin.add @codes
      @codes.add @cbox
      @codewin.show_all
      
      @cbox.signal_connect('key_press_event') { |w, e|
        if e.keyval == 65474
          begin
            result = eval @cbox.buffer.text
            @cout.buffer.text += "#{result}\n"
          rescue Exception => e  
            @cout.buffer.text += e.message+"\n"
            tag @cout, "#{e.message}", "err"
            for bt in e.backtrace
              @cout.buffer.text += "-- #{bt}\n"
            end
          end
          @cout.buffer.text += "---------------\n"
        end
      }
    end 
  end
  end
