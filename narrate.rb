#!/usr/local/bin/ruby
$ROOTDIR = "#{Dir.home}/.narrator.d/"
$EXTDIR = "#{$ROOTDIR}/extensions/"
$STORYDIR = "#{$ROOTDIR}/stories/"
$TITLE = "Narrator - %STORY% [%CHAPTER%:%SCENE%]"
$LOAD_PATH.unshift $ROOTDIR
$LOAD_PATH.unshift $EXTDIR

require 'opt'
require 'libkaiser'
require 'gtkp'
require "gtk2"
require "gtksourceview2"
require 'sqlite3'
# Core extensions
require 'libeval'
require 'libnarrator'
require 'libextension'
require 'libentities'

class DummyDB
  def initialize main
    @main = main
  end
  def query dummy, *extras
    dialog = Gtk::MessageDialog.new(@main, 
                                    Gtk::Dialog::DESTROY_WITH_PARENT,
                                    Gtk::MessageDialog::ERROR,
                                    Gtk::MessageDialog::BUTTONS_CLOSE,
                                    "No story is open.")
    dialog.run
    dialog.destroy
    nil
  end
  def execute dummy, *extras; query(dummy, extras);end
end


# todo
=begin
controls for add scene, add chapter, del scene, del chapter
add scene, opens prompt to choose owner chapter

add status bar for showing whic scene/chapter is open
=end
class NarratorGUI
  def error txt
    dialog = Gtk::MessageDialog.new(@root_window, 
                                    Gtk::Dialog::DESTROY_WITH_PARENT,
                                    Gtk::MessageDialog::ERROR,
                                    Gtk::MessageDialog::BUTTONS_CLOSE,
                                    txt)
    dialog.run
    dialog.destroy
  end
  def initialize
    init_narrator
    @openScene = nil
    
    @root_window = Gtk::Window.new
    $dummydb = DummyDB.new @root_window
    $db = $dummydb 
    @root_window.set_size_request(650, 500)
    @root_window.signal_connect('destroy') { Gtk.main_quit }

    @root_window << @layout = Gtk::Table.new(8, 8, false)
    @layout.attach(@mb = createMenuBar, 0, 8, 0, 1, Gtk::EXPAND|Gtk::FILL, Gtk::SHRINK)
 
    @layout.attach(@txts = Gtk::ScrolledWindow.new, 1, 8, 1, 7, Gtk::EXPAND|Gtk::FILL, Gtk::EXPAND|Gtk::FILL)
    @txts << @cbox = Gtk::SourceView.new
    @txts.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
    @cbox.show_line_marks = true
    @cbox.show_line_numbers = true
    @cbox.wrap_mode = Gtk::TextTag::WRAP_WORD

    @layout.attach(@chb = Gtk::ScrolledWindow.new, 0, 1, 1, 7, Gtk::EXPAND|Gtk::FILL, Gtk::EXPAND|Gtk::FILL)
    @chb.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
    @chapters = Gtk::TreeView.new(treestore = Gtk::TreeStore.new(String))
    @chapters.append_column(Gtk::TreeViewColumn.new("Chapter", Gtk::CellRendererText.new,  :text => 0))
    @chb.add @chapters
    @chapters.signal_connect("cursor-changed") do |v|
      # autosave if a scene is active in writer area here
      if v.selection.selected != nil
        s = v.selection.selected[0]

        id = getChapter(name: s).first[0]
        scenes = {}
        menu = Gtk::Menu.new
        ordy = 0
        $db.query("select * from scenes where chapter = #{id} order by ind").each { |r|
          menu.append(mi = Gtk::MenuItem.new("#{id}:#{ordy}:#{r[1]}"))
          puts "ADDING #{id}:#{ordy}:#{r[1]}"
          mi.signal_connect("activate") { |w|
            if @openScene != nil and @openScene[:start_text] != @cbox.buffer.text
              puts "Saved scene..."
              puts "--> #{@openScene}\n--> #{@cbox.buffer.text}\n<--"
              $db.execute "update scenes set body = ? where id = '#{@openScene[:id]}'", @cbox.buffer.text
            end
            chapter = w.label.split(":")[0]
            myord = w.label.split(":")[1]
            scene = w.label.split(":")[2]
            puts "#{w.label.split(':')}"
            scene = $db.query("select * from scenes where chapter = #{id} and ind = #{myord}").first
            
            puts scene
            @openScene = {id: scene[0], name: scene[1], chapter: scene[2], start_text: scene[4]}
            @cbox.buffer.text = "#{scene[4]}"
            updateStatusBar
          }
          ordy += 1
        }
        menu.append(addmi = Gtk::MenuItem.new("<Add Scene>"))
        addmi.signal_connect("activate"){
          openPrompt("New Scene", "Enter scene name: ") { |name| 
            puts "Adding scene #{name} to #{id}"
            ord = 0
            $db.query("select * from scenes where chapter = #{id}").each { |r| ord = r[3]+1 if ord <= r[3]}
            $db.execute "insert into scenes(id, name, chapter, ind, body, tags) values('#{randstr}', '#{name}', #{id}, #{ord}, '', '')"
          }
        }
        menu.show_all
        menu.popup(nil, nil, 0, Gdk::Event::CURRENT_TIME)
      end
    end
    
    @root_window.show_all
    
    init_extension_manager
   

    if @opt.command() != ""
      openStoryUI @opt.command()
    end
    Gtk.main
  end  

  
  def newStory
    dialog = Gtk::FileChooserDialog.new(
      "Choose destination to save story file to ...", @root_window, Gtk::FileChooser::ACTION_SAVE, nil,
      [ Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL ], [ Gtk::Stock::SAVE, Gtk::Dialog::RESPONSE_ACCEPT ]
    )
    filter = Gtk::FileFilter.new
    filter.add_pattern("*.db")
    dialog.add_filter filter
    dialog.current_folder = $STORYDIR
    dialog.signal_connect('response') do |w, r|
      odg = case r; when Gtk::Dialog::RESPONSE_ACCEPT; dialog.filename; else; nil; end
      dialog.destroy
      if odg != nil
        $db = SQLite3::Database.open odg
        $db.execute "CREATE TABLE IF NOT EXISTS characters(name TEXT NOT NULL)" # a default entity list
        $db.execute "CREATE TABLE IF NOT EXISTS entities(name TEXT)" # index of entity lists
        $db.execute "CREATE TABLE IF NOT EXISTS notes(notepad TEXT, tags TEXT)" # a notepad 
        $db.execute "CREATE TABLE IF NOT EXISTS scenes(id TEXT, name TEXT, chapter INTEGER, ind INTEGER, body TEXT, tags TEXT)"
        $db.execute "CREATE TABLE IF NOT EXISTS story(story_key TEXT, story_var TEXT)" # settings
        $db.execute "CREATE TABLE IF NOT EXISTS chapters (id INTEGER, name TEXT)"
        $db.execute "insert into entities(name) values ('characters')"
        $db.execute "insert into chapters(id, name) values (1, 'Prologue')"
        $db.execute "insert into scenes(id, name, chapter, ind, body, tags) values ('#{randstr}', 'Introductions', 1, 0, '', '')"
        $db.execute "insert into story(story_key, story_var) values ('author', '');"
        $db.execute "insert into characters(name) values ('protag');"
        openStoryUI odg, db: $db
      end
    end
    dialog.run
  end
  

  def getScene id: "_", name: "_"
    $db.query "select * from scenes where id = '#{id}' or name = '#{name}'"
  end
  
  def getChapter id: "_", name: "_"
    $db.query "select * from chapters where id = '#{id}' or name = '#{name}'"
  end

  def closeStory
    @storypath = nil
    @openScene = nil
    @openchapter = nil
    @chapters.model.clear
    $db = $dummydb
    updateStatusBar
  end
  def openStoryUI(path, db: nil)
    @selected = 0
    @buffers = {}
    $db = db unless db == nil
    $db = SQLite3::Database.open(path) if db == nil
    #@root_window.title = path.split("/")[-1]
    @openchapter = ""
    @storypath = path

    $db.query("select * from chapters order by id").each { |r| addToTree @chapters, r[1] }
    updateStatusBar
  end
  
  def promptOpenStoryUI # Change to selecting files, see about filtering by .db
    dialog = Gtk::FileChooserDialog.new("Open Story", @root_window, Gtk::FileChooser::ACTION_OPEN, nil,
                                        [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
                                        [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])

    filter = Gtk::FileFilter.new
    filter.add_pattern("*.db")
    dialog.add_filter filter
    dialog.current_folder = $STORYDIR
    if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT; openStoryUI dialog.filename;end;dialog.destroy;
  end
end


if __FILE__ == $0
  NarratorGUI.new
end
