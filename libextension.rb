
class NarratorGUI
  def init_extension_manager
    @extensions = {}
    @statusbar_dynamics = {}
    @extmenus = {}
    loadExtensions
  end
  
  def openExtensionManager
    @extwin = Gtk::Window.new
    @extwin.set_size_request(350, 150)
    @extwin << @extlay = Gtk::ScrolledWindow.new
    @extlay.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS)
    @hb = Gtk::VBox.new
    @extlay.add_with_viewport @hb
    
    Dir.foreach($EXTDIR) do |entry|
      if entry.end_with? ".rb"
        e = Gtk::Chec   kButton.new(entry.split(".")[0])
        if @extensions[entry.split(".")[0]][:enabled]
          e.active = true
        end
        e.signal_connect( "toggled" ) { |w|
          if w.active?
            send "load_#{w.label}", self
            @extensions[entry.split(".")[0]][:enabled] = true
          else
            send "unload_#{w.label}", self
            @extensions[entry.split(".")[0]][:enabled] = false
          end
        }
        @hb.add e
      end
    end
    @extwin.show_all
  end

  def loadExtensions
    blacklist = []
    autos = []
    blacklist = @config["internals"]["blacklist"] unless @config["internals"]["blacklist"] == nil
    autos = @config["internals"]["autoloads"] unless @config["internals"]["autoloads"] == nil
    Dir.foreach($EXTDIR) do |entry|
      if entry.end_with? ".rb"
        puts "Loading extension #{entry} in to memory..."

        unless blacklist.include? entry.split(".")[0]
          @extensions[entry.split(".")[0]] = {ui_elements: {}, enabled: false}
          require entry.split(".")[0]

          if autos.include? entry.split(".")[0]
            send "load_#{entry.split(".")[0]}", self
            @extensions[entry.split(".")[0]][:enabled] = true
          end
        end
      end
    end
  end

  
  def handleStatusMsg txt;
    #puts $TITLE
    #puts txt
    t = $TITLE.dup;
    t.gsub! "%STORY%", "None" if @storypath == nil
    t.gsub! "%STORY%", "#{@storypath}" unless @storypath == nil
    if @openScene != nil
      t.gsub! "%CHAPTER%", "#{@openScene[:chapter]}" 
      t.gsub! "%SCENE%", "#{@openScene[:name]}"
    else
      t.gsub! "%CHAPTER%", "none" 
      t.gsub! "%SCENE%", "none"
    end
    t;
  end

  def updateStatusBar; @root_window.title = handleStatusMsg($TITLE); end

  
  # API methods
  def addExtButton(extension_id, name, &fn)
    n = Gtk::MenuItem.new(name)
    n.signal_connect('activate') { |w| fn.call(self) }
    @ext_menu.append n
    n.show
    @extensions[extension_id][:ui_elements]["mb_"+name] = n
  end
  
  def delExtButton(extension_id, name)
    @extensions[extension_id][:ui_elements]["mb_"+name].destroy()
    @extensions[extension_id][:ui_elements]["mb_"+name] = nil
  end
  
  def changeTxtFont(txt, fontname)
    font = Pango::FontDescription.new(font)
    txt.modify_font(font)
  end





  
  def autoloadExtension(ext_id)
    @config["internals"]["autoloads"].append ext_id
    writeConfig
  end
  
  def unautoloadExtension(ext_id)
    @config["internals"]["autoloads"].delete ext_id
    writeConfig
  end
  
  def blacklistExtension(ext_id)
    @config["internals"]["blacklist"].append ext_id
    writeConfig
  end
  
  def unblacklistExtension(ext_id)
    @config["internals"]["blacklist"].delete ext_id
    writeConfig
  end
end
