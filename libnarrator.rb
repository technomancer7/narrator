require "json"
$all_txt = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten

def randstr(lim=20); (0...lim).map { $all_txt[rand($all_txt.length)] }.join;end

class NarratorGUI
  def init_narrator
    @opt = OptHandler.new
    if not File.exist?($ROOTDIR); Dir.mkdir($ROOTDIR); end
    if not File.exist?($EXTDIR); Dir.mkdir($EXTDIR); end
    if not File.exist?($STORYDIR); Dir.mkdir($STORYDIR); end
    if not File.exist?("#{$ROOTDIR}config.json")
      File.open("#{$ROOTDIR}config.json", "w+") { |file| file.write("{}") }
      @config = {}
      @config["theme"] = {}
      @config["theme"]["main_txt_font"] = "Monospace 9"
      @config["internals"] = {}
      @config["internals"]["blacklist"] = []
      @config["internals"]["autoloads"] = []
      writeConfig
    else
      @config = JSON.parse(File.read("#{$ROOTDIR}config.json"))
    end
  end
  
  def setv(grp, key, value)
    @config[grp] = {} unless @config[grp].key? key; @config[grp][key] = value; writeConfig
  end
  
  def getv(grp, key, default)
    @config[grp] = {} unless @config.key? grp
    return @config[grp][key] if @config[grp].key? key
    default
  end
  
  def writeConfig; File.open("#{Dir.home}/.narrator.d/config.json", "w+") { |file| file.write(@config.to_json) }; end
  
  def createMenuBar
    @menubar = Gtk::MenuBar.new

    filemenu = Gtk::Menu.new; editmenu = Gtk::Menu.new; extmenu = Gtk::Menu.new; helpmenu = Gtk::Menu.new
    @menubar.append(file = Gtk::MenuItem.new("File")); @menubar.append(edit = Gtk::MenuItem.new("Edit"))
    @menubar.append(ext = Gtk::MenuItem.new("Extend")); @menubar.append(help = Gtk::MenuItem.new("Help"))
    file.submenu = filemenu; edit.submenu = editmenu; ext.submenu = extmenu; help.submenu = helpmenu
    @extmenu = extmenu
    
    group = Gtk::AccelGroup.new
    filemenu.append(new = Gtk::ImageMenuItem.new(Gtk::Stock::NEW, group))
    filemenu.append(open = Gtk::ImageMenuItem.new(Gtk::Stock::OPEN, group))
    #filemenu.append(save = Gtk::ImageMenuItem.new(Gtk::Stock::SAVE, group))
    filemenu.append(achp = Gtk::MenuItem.new("Add Chapter"))
    filemenu.append(dchp = Gtk::MenuItem.new("Delete Chapter"))
    filemenu.append(clss = Gtk::MenuItem.new("Close Story"))
    new.signal_connect('activate') { |w| newStory }
    open.signal_connect('activate') { |w| promptOpenStoryUI }
    clss.signal_connect('activate') { |w| closeStory }
    #save.signal_connect('activate') { |w| saveStory  }

    achp.signal_connect('activate') { |w|
      openPrompt("New Chapter", "Enter chapter name: ") { |name|
        ord = 0
        $db.query("select * from chapters").each { |r| ord = r[0]+1 if ord <= r[0]}
        $db.execute "insert into chapters(id, name) values('#{ord}', '#{name}')"
        @chapters.model.clear
        $db.query("select * from chapters order by id").each { |r| addToTree @chapters, r[1] }
      }
    }

    dchp.signal_connect('activate') { |w|
      ask("Delete #{@openScene[:chapter]}", "Delete chapter #{@openScene[:chapter]}? \nThis can't be undone!") { |r|
        if r
          # TODO
        end
      }
    }
    
    editmenu.append(npad = Gtk::MenuItem.new("Notepad"))
    editmenu.append(setv = Gtk::MenuItem.new("Settings"))
    editmenu.append(entv = Gtk::MenuItem.new("Open Entity Viewer"))
    editmenu.append(sqv = Gtk::MenuItem.new("Open SQL Runner"))
    editmenu.append(fontv = Gtk::MenuItem.new("Set Storyview Font"))
    sqv.signal_connect('activate') { |w| sqlRunner }   
    npad.signal_connect('activate') { |w| openNotepad }
    setv.signal_connect('activate') { |w| f = HashEditor.new @config; f.callback { |jso| @config = jso; writeConfig; };}

    entv.signal_connect('activate') { |w| openEntities }
    fontv.signal_connect('activate') { |w| puts "TODO: Open font view" }
    
    extmenu.append(extb = Gtk::MenuItem.new("Extensions"))
    extmenu.append(extev = Gtk::MenuItem.new("Live Console"))
    
    extb.signal_connect('activate') { |w| openExtensionManager }
    extev.signal_connect('activate') { |w| openEval }
   
    
    vbox = Gtk::VBox.new(homogeneous = false, spacing = nil)
    vbox.pack_start_defaults(@menubar)
    return vbox
  end


  def sqlRunner
    w = Gtk::Window.new
    w.set_size_request(350, 200)
    w << sqlayout = Gtk::Table.new(8, 8, false)
    sqlayout.attach(en = Gtk::Entry.new, 0, 7, 0, 1, Gtk::EXPAND|Gtk::FILL, Gtk::SHRINK)
    sqlayout.attach(snd = Gtk::Button.new("Execute"), 7, 8, 0, 1, Gtk::EXPAND|Gtk::FILL, Gtk::SHRINK)

    sqlayout.attach(txts = Gtk::ScrolledWindow.new, 0, 8, 1, 8, Gtk::EXPAND|Gtk::FILL, Gtk::EXPAND|Gtk::FILL)
    
    txts << cb = Gtk::SourceView.new
    txts.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
    cb.show_line_marks = true
    cb.show_line_numbers = true
    cb.wrap_mode = Gtk::TextTag::WRAP_WORD
    def commit en, cb
      cb.buffer.text = ""
      $db.query(en.text).each { |r|
        cb.buffer.text = "#{r}\n#{cb.buffer.text}"
      }
    end
    en.signal_connect('key_press_event') { |w, e| if e.keyval == 65293;commit en, cb;end; }
    snd.signal_connect("clicked") {
      commit en, cb
    }
    w.show_all
  end
  
  def openNotepad #change to ave a tree view of 
    if @notepad == nil
      @notepad = Gtk::Window.new
      @notepad_container = Gtk::ScrolledWindow.new
      @notepad_text = Gtk::SourceView.new
      @notepad_text.show_line_marks = true
      @notepad_text.show_line_numbers = true
      @notepad_text.wrap_mode = Gtk::TextTag::WRAP_WORD
      @notepad_container.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS)
      @notepad.add @notepad_container
      @notepad_container.add @notepad_text
      @notepad.set_size_request(350, 400)
      @notepad.signal_connect('destroy') {

        # TODO andle saving
        @notepad = nil
      }
      
      # TODO load from db
      
      @notepad.show_all
    end
  end  
end
