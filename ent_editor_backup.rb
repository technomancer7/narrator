  def openEntities main: nil
    @ent_view = Gtk::Window.new if main == nil
    @ent_view = main unless main == nil
    @ent_view << @ent_ls = Gtk::VBox.new
    
    grps = {}
    $db.query("select * from entities").each { |r|
      @ent_ls << grps[r[0]] = Gtk::Button.new(r[0])
      grps[r[0]].signal_connect("clicked") { |w|
        #@ent_ls.destroy
        $db.results_as_hash = true
        lbl = w.label.dup
        ents = {}
        @ent_ls.destroy
        @ent_view << @ent_ls = Gtk::VBox.new
        all_ents = $db.query("select * from #{lbl}")
        all_ents.each { |h|
          ents[h["name"]] = {button: Gtk::Button.new(h["name"]), table: h.dup}
          @ent_ls << ents[h["name"]][:button]
          ents[h["name"]][:button].signal_connect("clicked") { |w|
            @ent_ls.destroy
            @ent_view << @ent_ls = Gtk::VBox.new
            @ent_ls << hb = Gtk::HBox.new
            hb << Gtk::Label.new("Key")
            hb << Gtk::Label.new("Value")
            props = {}
            ents[w.label][:table].each { |k, v|
              puts "#{k}: #{v}"
              props[k] = {entry_key: Gtk::Entry.new, entry_value: Gtk::Entry.new, key: k, value: v}
              
              @ent_ls << hb = Gtk::HBox.new
              hb << props[k][:entry_key]
              props[k][:entry_key].text = "#{k}"
              hb << props[k][:entry_key]
              props[k][:entry_value].text = "#{v}"
            }

            @ent_ls << conf = Gtk::Button.new("< Confirm")
            
            
          }
          
        }
        @ent_ls << addb = Gtk::Button.new("< BACK")
        addb.signal_connect("clicked") {
          @ent_ls.destroy
          openEntities main: @ent_view
          
        }
        @ent_view.show_all
        $db.results_as_hash = false
      }
    }

    @ent_ls << ctrls = Gtk::HBox.new
    ctrls << addb = Gtk::Button.new("+")
    addb.signal_connect("clicked") {
      openPrompt("New entity group", "Enter name for new group:"){ |newname| 
        $db.execute("insert into entities(name) values ('#{newname}')")
        $db.execute "CREATE TABLE IF NOT EXISTS #{newname}(name TEXT)"
        @ent_view.destroy
        openEntities
      }
      
    }
    @ent_view.show_all
  end
