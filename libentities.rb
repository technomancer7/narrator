class NarratorGUI
  def saveEntityTable
    if @currentEntGroup != nil
      $db.execute "delete from #{@currentEntGroup}"
      keys = @ent_editor.buffer.text.split("\n")[0].split(",")
      keys.map!(&:strip)
      actuals = @ent_editor.buffer.text.split("\n")[1..-1]
      for l in 0..actuals.length - 1
        ln = actuals[l]
        values = ln.split(",")
        values.map!(&:strip)
        outln = "insert into #{@currentEntGroup} (#{keys.join(', ')}) values"
        props = []
        for i in 0..keys.length-1
          props << "'#{values[i]}'"
        end

        outln << "(#{props.join(', ')})"
        $db.execute outln
      end
      @currentEntGroup = nil
    end
  end
  
  def openEntities
    if @storypath == nil
      error "No story open."
      return
    end
    @ent_view = Gtk::Window.new if @ent_view == nil
    
    @ent_view.signal_connect('destroy') { @ent_view = nil;@ent_dbl = nil }
    
    @ent_view.set_size_request(450, 300)
    @oldEntData = []
    @ent_view << @ent_tbl = Gtk::Table.new(4, 5, false)
    @ent_tbl.attach(@ent_grp_ctrls = Gtk::HBox.new, 0, 2, 4, 5, Gtk::EXPAND|Gtk::FILL, Gtk::SHRINK)
    @ent_tbl.attach(@ent_col_ctrls = Gtk::HBox.new, 2, 4, 4, 5, Gtk::EXPAND|Gtk::FILL, Gtk::SHRINK)

    @ent_grp_ctrls << @ent_grp_add = Gtk::Button.new("New Group")
    @ent_grp_add.signal_connect('clicked') {
      openPrompt("New entity group", "Enter name for new group:"){ |newname| 
        $db.execute("insert into entities(name) values ('#{newname}')")
        $db.execute "CREATE TABLE IF NOT EXISTS #{newname}(name TEXT)"
        @ent_view.destroy
        openEntities
      }
    }
    @ent_grp_ctrls << @ent_grp_rem = Gtk::Button.new("Remove Group")
    @ent_grp_rem.signal_connect("clicked"){
      ask("Really delete #{@currentEntGroup}?", "Delete #{@currentEntGroup}?") { |r|
        if r
          puts "ok deleting"
          $db.execute "drop table #{@currentEntGroup}"
          $db.execute "delete from entities where name = '#{@currentEntGroup}'"
          @ent_tbl.destroy
          openEntities
        end
      }
    }
    @ent_col_ctrls << @ent_col_add = Gtk::Button.new("New Column")
    @ent_col_add.signal_connect("clicked"){
      openPrompt("New entity column", "Enter name for new column:"){ |newname| 
        $db.execute "ALTER TABLE #{@currentEntGroup} ADD COLUMN #{newname} TEXT"
        @ent_view.destroy
        openEntities
      }
    }
    
    @ent_col_ctrls << @ent_col_rem = Gtk::Button.new("Remove Column")
    @ent_col_rem.signal_connect("clicked"){
      all_cols = []
      menu = Gtk::Menu.new
      $db.query("PRAGMA table_info(#{@currentEntGroup})").each{ |r|
        all_cols << r[1]
        menu.append(mi = Gtk::MenuItem.new("#{r[1]}"))
        mi.signal_connect("activate") { |w|
          puts "Remove label #{w.label}"
          all_cols -= [w.label]
          puts all_cols
          $db.execute "CREATE TABLE tmp AS SELECT #{all_cols.join(', ')} FROM #{@currentEntGroup}"
          $db.execute "DROP TABLE #{@currentEntGroup}"
          $db.execute "ALTER TABLE tmp RENAME TO #{@currentEntGroup}"
          @ent_view.destroy
          openEntities
        }
      }
      menu.show_all
      menu.popup(nil, nil, 0, Gdk::Event::CURRENT_TIME)
      
    }
        
    @ent_col_ctrls << @ent_col_sv = Gtk::Button.new("Save")
    @ent_col_sv.signal_connect('clicked') { saveEntityTable }
    @ent_tbl.attach(@ent_editor_sc = Gtk::ScrolledWindow.new, 1, 4, 0, 4, Gtk::EXPAND|Gtk::FILL, Gtk::EXPAND|Gtk::FILL)
    @ent_editor_sc.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
    @ent_editor_sc << @ent_editor = Gtk::TextView.new
    @ent_tbl.attach(@ent_ls = Gtk::ScrolledWindow.new, 0, 1, 0, 4, Gtk::EXPAND|Gtk::FILL, Gtk::EXPAND|Gtk::FILL)
    @ent_ls.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
    @ent_tree = Gtk::TreeView.new(treestore = Gtk::TreeStore.new(String))
    @ent_tree.append_column(Gtk::TreeViewColumn.new("Entities", Gtk::CellRendererText.new,  :text => 0))
    @ent_ls.add @ent_tree
    @ent_tree.signal_connect("cursor-changed") do |v|
      if v.selection.selected != nil
        s = v.selection.selected[0]
        @currentEntGroup = s
        all_ents = $db.query("select * from #{s}")
        keys = []
        $db.query("PRAGMA table_info(#{s})").each{ |r|
          keys << r[1]
        }
        @ent_editor.buffer.text = "#{keys.join(', ')}\n"
        all_ents.each do |h|
          h.map { |x| x == nil ? '' : x }
          @oldEntData << h
          @ent_editor.buffer.text = "#{@ent_editor.buffer.text}#{h.join(', ')}\n"
        end
      end
    end
    
    grps = {}
    $db.query("select * from entities").each { |r| addToTree @ent_tree, r[0] }


=begin
    @ent_ls << ctrls = Gtk::HBox.new
    ctrls << addb = Gtk::Button.new("+")
    addb.signal_connect("clicked") {
      openPrompt("New entity group", "Enter name for new group:"){ |newname| 
        $db.execute("insert into entities(name) values ('#{newname}')")
        $db.execute "CREATE TABLE IF NOT EXISTS #{newname}(name TEXT)"
        @ent_view.destroy
        openEntities
      }
      
    }
=end
    @ent_view.show_all
  end
end
